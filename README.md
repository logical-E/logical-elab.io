# changeable.gitlab.io

**A website to inform people, from Edward (me).**
<!--

[//]: # "This is a comment. License mark HTML (CC BY-SA 4.0 term - Idiomatic element, Title of my work - Strong Importance, bold text element): <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><strong><a property="dct:title" rel="cc:attributionURL" href="https://unique-view.gitlab.io/">A website to inform people, from Edward (me)</a></strong> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/unique-view">Edward Jones</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;"><i>CC BY-SA 4.0</i><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a><br />This work may use third-party content to which that license does not apply (it is not licensed under that).</p>"

-->
This is the READ-ME file of my project, a website *"for knowledge & other ideas to inform people"*, \
licensed by me, Edward Jones under *CC BY-SA 4.0*. This work may use third-party content,\
to which that license does not apply (it is not licensed under that). To view a copy of that license, visit http://creativecommons.org/licenses/by-sa/4.0/ .

Versions: • https://unique-view.gitlab.io/ - main (stable) release • ??? - (open) beta release

\
I want to have a non-perfect design with content in another directory, abstracted from the Static Site Generator (SSG) part. 
 
 | content                        | Static Site Generator |
 | ------------------------------ | --------------------- |
 | other folder for web content   | website submodule (?) |

\
The [_Jekyll_](https://jekyllrb.com/docs/) SSG, processes (static) web page content with templates, written in markup language\
and _Liquid_ (template engine) embedded *templating code* (placeholder text to insert information),\
to make web pages.

* `_includes` folder - content pieces to include in other files
* `_layouts/default.html` - base template used by site pages

I was thinking the SSG can be [_VitePress_](https://vitepress.dev/guide/what-is-vitepress) - single and multi- page generator.

\
Site appearance (minimal CSS framework) is [_Pure_](https://purecss.io/).

\
Modern browser progressive experience ("basic content first")\
front-end framework to use is [_petite-vue_](https://github.com/vuejs/petite-vue) (subset of _Vue.js_).

\
To run the website locally on a Raspberry Pi, Debian Linux distribution, I need:

 1. _Apache HTTP Server_ (2.4) [installed](https://httpd.apache.org/docs/2.4/install.html#overview)
    * if installed from source, install [apache2.service](apache2.service) (**line 6**) to run
      [**after system boot**](https://httpd.apache.org/docs/2.4/invoking.html#boot).
    * edit service unit file (`sudo systemctl edit --full apache2`) - replace\
      `apachectl start` with `apachectl -c "ServerName 127.0.0.1" -k start` and\
      `apachectl graceful` with `apachectl -c "ServerName 127.0.0.1" -k graceful`.\
      &ZeroWidthSpace;

 2. _ngrok_, a service to securely access the local _Apache_ web server\
    \- (`sudo`) install **as a [system service](https://ngrok.com/docs/secure-tunnels/ngrok-agent/installing-as-a-service/#installation)**
    and use a _ngrok_ (sub)domain.\
    &ZeroWidthSpace;

 3. _GitLab Runner_ to run _CI/CD_ job commands in a runner\
    (the _shell_ executor, tags to select runner) in a _Docker Engine_ container
    * At regular, scheduled times, if changes needed: download the artifact files of the\
      latest job, that builds the website pages and move this to the server configuration\
      _DocumentRoot_. Use _cron_ (`sudo crontab -e` - read `man`ual).
