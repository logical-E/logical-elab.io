---
title: Computers - my simple definition
excerpt: Computers are made of software programs (system, app) and hardware (physical computer parts)...
layout: notebook
custom_styling: >
  body > main > * {
    text-align: center;
  }

  body > main > h2 {
    margin-block: 0.9em 0.1em;
  }

  body > main > section {
    --heading-pad: 1.2em;

    margin-block-start: var(--heading-pad);
    padding: 0.6em;
    padding-block-start: var(--heading-pad);
  
    border: 0.1em solid currentcolor;
    border-radius: 0.8em;
    border-block-start-width: 0;
  }

  body > main > section > h3 {
    position: relative;

    margin-block-start: -moz-calc(var(--heading-pad) * (1/1.17) * -1.5);
    margin-block-start: -webkit-calc(var(--heading-pad) * (1/1.17) * -1.5);
    margin-block-start: calc(var(--heading-pad) * (1/1.17) * -1.5);
    margin-block-end: 0;

    white-space: nowrap;
    overflow: hidden;

    border-block: inherit;
    border-block-end-width: 0;
  }

  body > main > section > h3::after,
      body > main > section > h3::before {
    /* "----- Software -----" */
    content: "";
    display: inline-block;
    position: absolute;
    top: 0.5em;
    width: 48%;
    transform: translateX(0.45em);

    border-block: inherit;
    border-block-start-width: 0.1em;
  }

  body > main > section > h3::before {
    transform: translateX(-moz-calc(-100% - 0.45em));
    transform: translateX(-webkit-calc(-100% - 0.45em));
    transform: translateX(calc(-100% - 0.45em));
  }

  body > main > section dl > dd {
    margin-inline: 0.8em;
  }

  body > main > section .system-software {
    max-width: 22em;
  }

  body > main > section .system-software,
      body > main > section .system-software ul {
    margin: 1.4em auto 1.5em auto;
    padding: 0;
  }

  body > main > section .system-software li {
    list-style-type: none;

    border: 0.2em solid gray;
    border-radius: 2em;

    margin-top: 0.8em;
    padding: 1.4em 1.6em 1.28em;
  }
---
## Computers

my simple definition about one\
  &#8203;

**They are built from**\
*Software* programs (system, apps) and *Hardware* parts...

<section>

  <h3>Software</h3>
  <dl>
    <dt>
      <h4>Software is...</h4>
    </dt>
    <dd>
    <p>Data and computer programs, these are made of instructions for a
    computer to read and do ("code"), written in a programming language.
    </p>
    <p>That word "instructions" (information about how to do something) is used
    <a href="https://www.google.com/search?hl=en&as_q=computer+program&as_epq=instructions&as_oq=what+is+a&as_eq=&as_nlo=&as_nhi=&lr=&cr=&as_qdr=all&as_sitesearch=&as_occt=any&as_filetype=&tbs=">
    many times on the web</a> for these programs.</p>
    </dd>
  </dl>

  <div>
    <h4>
      <em>system</em> software programs, a "platform for other software"
    </h4>
    <ul class="system-software" role="list">
      <li>(device) drivers, to control hardware</li>
      <li>
        operating system (it does management): Unix-like / BSD / Mac , Microsoft Windows
        <ul role="list">
          <li>with kernel code at its core</li>
        </ul>
      </li>
    </ul>
    <dl>
      <dt>
        <h4><em>application</em> software</h4>
      </dt>
      <dd>Programs "to perform a task".</dd>
    </dl>
  </div>

</section>

<section>

  <h3>Hardware</h3>
  <dl>
    <dt>
      <h4>Hardware is...</h4>
    </dt>
    <dd>
    The physical computer parts (input devices, central processing unit, memory, output devices).
    </dd>
  </dl>

</section>

\- [Computer Basics: What is a Computer?](https://edu.gcfglobal.org/en/computerbasics/what-is-a-computer/1/),
[Computer - Wikipedia, The Free Encyclopedia](https://en.wikipedia.org/wiki/Computer), [Catalog Home \| Codecademy](https://www.codecademy.com/catalog)\
  &#8203;