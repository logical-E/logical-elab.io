---
title: A circle concept (general idea)
excerpt: All things can be viewed as being one 'circular' whole, able to be divided..., able to be combined... (a system).
layout: notebook
custom_styling: >
  body > main > section {
    margin-inline: auto;
    max-width: 28em;
  }

  body > main > section > h2 {
    text-decoration: underline;
    text-underline-offset: 0.32em;
    line-height: 1.3;
    padding-block-end: 0.5em;
  }

  body > main > section > .shape {
    position: relative;
    margin-bottom: 1.35em;

    /* CSS responsive square box */
    width: 54%;
    height: 0;
    padding-top: 54%;

    background-color: teal;
    border: 0.07em solid currentcolor;
    border-radius: 50%;
  }

  body > main > section > .shape > * {
    position: absolute;
    top: 0;
    left: 0;
    margin: 0;

    /*  Content box, a cyclic or inscribed square. ^, ** = exponent
    - quadrant (1/4) circle sector of ".shape" - padding
      1. Right-angled triangle adjacent side, % length
        a = b = side of (1/4) square            (adjacent sides length)
        c = radius of circle = 50               (%, hypotenuse length)
        c ^ 2                = a ^ 2 + b ^ 2    (Pythagorean theorem)
        50 ^ 2               = a ^ 2 + a ^ 2    (a = b)
        ( 50 ^ 2 ) / 2       = a ^ 2
        square root { ( 50 ^ 2 ) / 2 } = a
        > Math.sqrt( ( 50 ** 2 ) / 2 )

      2. Radius of circle subtract (-)
        right-angled triangle adjacent side, % length
        r, radius of circle  = 50               (% length)
        r - a                = 50 - square root { ( 50 ^ 2 ) / 2 }
        > 50 - Math.sqrt( ( 50 ** 2 ) / 2 )

    - % height length = 2 * Math.sqrt( ( 50 ** 2 ) / 2 )
    */
    padding: 14.644660940672622%;
    height: 70.71067811865476%;
    text-align: center;
  }

  body > main > section > .shape > :not([class]) {
    z-index: 1;
    color: #ffaa2f;
  }

  body > main > section > .shape > .outer-position {
    box-sizing: border-box;
    top: 33.333%;
    left: 100%;
    width: -moz-calc(( (100 - 55) / 55 ) * 100% - 0.07em);
    width: -webkit-calc(( (100 - 55) / 55 ) * 100% - 0.07em);
    width: calc(( (100 - 55) / 55 ) * 100% - 0.07em);

    height: 33.333%;
    padding: 0.35em;
  }

  body > main > section > .shape > div.sector {
    box-sizing: border-box;
    top: 50%;
    left: 50%;
    margin-top: 0.35em;
    margin-left: 0.35em;
    width: 50%;
    height: 50%;
    padding: 0;

    /* 0.07 × 2 */
    border-width: 0.14em;
    border-style: inherit;
    border-color: inherit;
    background-color: darkslateblue;
    -webkit-border-bottom-right-radius: 100%;
    border-bottom-right-radius: 100%;
  }

  body > main > section > .shape > div.sector + .outer-position {
    top: 66.666%;
  }
---

<section>

  <h2>A circle concept (general idea)</h2>

  <p>All things can be viewed as being a system...</p>

  <div class="shape">
    <p>
      <strong>one "circular" whole</strong> (similar to or like a complete circle)
    </p>
    <p class="outer-position">
      - able to be <em>divided</em> into
    </p>
    <div class="sector"></div>
    <p class="outer-position">
      component parts (like a circle sector)
    </p>
  </div>

  <p>- and able to be <em>combined</em> to make whole things.</p>

</section>