---
excerpt: 'Shared notebook.'
layout: notebook
custom_styling: >
  /*
  @media only all and (min-width: 45em) {
    body > p, body > nav {
      box-sizing: border-box;
    }
    body > nav {
      width: 45%;
    }
    body > p {
      float: right;
      width: 54.9%;
      margin-block: 0;
      padding-block: 0.6em;
      text-align: revert;
      border-left: 0.07em solid rgb(0, 0, 0, 35%);
    }
  }
  */
---

{%- comment %} Content code (Liquid)...
<!-- Inline CSS |
                V {% endcomment -%}
{%- capture item_styling %}box-sizing: border-box; vertical-align: top; width: {{ 100 | divided_by: site.notebook_pages.size }}%;{%- endcapture -%}
{%- assign item_end_padding = " padding-inline-end: -webkit-calc(var(--book-border-width) * 2);" -%}
{%- assign item_end_padding = item_end_padding | append: " padding-inline-end: -moz-calc(var(--book-border-width) * 2);" -%}
{%- assign item_end_padding = item_end_padding | append: " padding-inline-end: calc(var(--book-border-width) * 2);" -%}

{%- capture nav_list -%}
<ul>{%- endcapture -%}
{%- for p in site.notebook_pages %}
  
  {%- capture nav_list -%}{{ nav_list }}<li style="{{ item_styling }}{%- endcapture -%}
  {%- if forloop.index < forloop.length -%}
    {%- assign nav_list = nav_list | append: item_end_padding -%}
  {%- endif -%}
  {%- capture nav_list -%}{{ nav_list }}">
  <a href="{{ p.url }}" target="embedded">{{ p.title | escape }}</a>
</li>{%- endcapture -%}
{% endfor -%}

{%- capture nav_list -%}{{ nav_list }}</ul>{%- endcapture -%}
{%- comment %} --> End of code. {% endcomment %}

<div id="skip">
  <a href="#notebook-nav">Skip to navigation of notebook pages on the right</a>
</div>

<div class="notebook">
  <!-- Sorted out layout problems? -->
  <div class="outer">
    <div>
      <!--suppress HtmlUnknownTarget -->
      <iframe title="open page of notebook (sub-page)" src="/notebook_pages/index.html" name="embedded"></iframe>
    </div>
  </div>
  <div class="outer">
    <nav id="notebook-nav">
      <h3>&NonBreakingSpace;Page navigation&NonBreakingSpace;</h3><p>-&NonBreakingSpace;has the look of notebook divider tabs&NonBreakingSpace;</p>
      <br>{{ nav_list }}
    </nav>
  </div>
</div>